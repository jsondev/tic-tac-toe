var gameboard = require('../../client/js/gameboard');
var endgame = require('../../client/js/endgame');

module.exports = {
    validate: function (player) {
        // Checks to see if a player has one by having 3 marks consecutively in a row and ends game if true
        switch (true) {
            case gameboard.board[0] === player &&
            gameboard.board[1] === player &&
            gameboard.board[2] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[3] === player &&
            gameboard.board[4] === player &&
            gameboard.board[5] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[6] === player &&
            gameboard.board[7] === player &&
            gameboard.board[8] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[0] === player &&
            gameboard.board[3] === player &&
            gameboard.board[6] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[1] === player &&
            gameboard.board[4] === player &&
            gameboard.board[7] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[2] === player &&
            gameboard.board[5] === player &&
            gameboard.board[8] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[0] === player &&
            gameboard.board[4] === player &&
            gameboard.board[8] === player:
                endgame.addScore(player);
                break;
            case gameboard.board[2] === player &&
            gameboard.board[4] === player &&
            gameboard.board[6] === player:
                endgame.addScore(player);
                break;
            default:
                break;
        }
    }
}