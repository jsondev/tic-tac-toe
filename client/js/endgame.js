var gameboard = require('./gameboard');

module.exports = {
    addScore: function (player) {
        if (player === "X") {
            // Adds score to X Player ends the game
            gameboard.score.playerOne += 1;
            gameboard.gameOver = true;
        } else if (player === "O") {
            // Adds score to O Player ends the game
            gameboard.score.playerTwo += 1;
            gameboard.gameOver = true;
        }
    },

}