module.exports = {
  board: ["", "", "", "", "", "", "", "", ""],
  score: {
    playerOne: 0,
    playerTwo: 0
  },
  label: {
    start: "Start Game",
    reset: "Reset game",
    playerOne: "X",
    playerTwo: "O"
  },
  gameOver: false
}