var express = require('express');

var app = express();
var serv = require('http').createServer(app);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/index.html');
});
app.use('/client', express.static(__dirname + '/client'));

serv.listen(2000);
var gameboard = require('./client/js/gameboard');
var validate = require('./server/js/validate');

var SOCKET_LIST = {};
var PLAYER_LIST = {};

var playerSelect = ['one', 'two'];

var io = require('socket.io')(serv, {});
io.sockets.on('connection', function (socket) {
    socket.id = Math.random();
    SOCKET_LIST[socket.id] = socket;



    // Sets the players on connection
    if (playerSelect.length >= 1) {
        socket.emit('connection-server', {
            player: playerSelect.pop()
        });
    }

    // Starts the game
    socket.on('start-client', function () {
        gameboard.board = ["", "", "", "", "", "", "", "", ""];
        gameboard.gameOver = false;
        io.emit('start-server', {
            reset: gameboard.label.reset,
            startingPlayer: "one",
            clear: gameboard.board
        });
    });

    // Switches players after a move
    socket.on('playermove-client', function (data) {
        if (data.player === "one") {
            gameboard.board.splice(data.selected, 1, data.marker);
            validate.validate(data.marker);
            io.emit('playermove-server', {
                player: gameboard.label.playerOne,
                move: data.selected,
                marker: data.marker,
                opponant: data.opponant,
                score: gameboard.score.playerOne,
                gameOver: gameboard.gameOver
            });
        } else if (data.player === "two") {
            gameboard.board.splice(data.selected, 1, data.marker);
            validate.validate(data.marker);
            io.emit('playermove-server', {
                player: gameboard.label.playerTwo,
                move: data.selected,
                marker: data.marker,
                opponant: data.opponant,
                score: gameboard.score.playerTwo,
                gameOver: gameboard.gameOver
            });
        }
    });

    // Resets the score
    socket.on('reset score', function (data) {
        gameboard.score.playerOne = data.playerOne;
        gameboard.score.playerTwo = data.playerTwo;
        gameboard.gameOver = data.gameOver;
        io.emit('reset score');
    });

    // Removes players on disconnect
    socket.on('disconnect', function () {
        delete SOCKET_LIST[socket.id];
        delete PLAYER_LIST[socket.id];

    });
});